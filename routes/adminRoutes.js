const { Router } = require("express");
const router = Router();
const adminController = require("../controllers/adminControllers");

// All routes are valid only when user will be authenticated which we do via middleware used in app.js
router.patch("/admin/:id/edit", adminController.updateAdmin);
router.patch("/admin/user/:id/edit", adminController.updateUser);
router.delete("/admin/:adminId/user/:id/delete", adminController.deleteUser);
router.post("/admin/user/new", adminController.newUser);
router.patch("/admin/restaurant/:id/edit", adminController.updateRestaurant);
router.delete("/admin/restaurant/:id/delete", adminController.deleteRestaurant);
router.post("/admin/:id/restaurant/create", adminController.newRestaurant);

module.exports = router;
