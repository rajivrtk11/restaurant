const { Router } = require("express");
const router = Router();
const userController = require("../controllers/userControllers");

// route is valid only when user will be authenticated which we do via middleware used in app.js

router.patch("/user/:id/edit", userController.updateUser);
router.get("/user/:id/restaurants", userController.getRestauarants);
router.get("/restaurants/filter", userController.filter);

module.exports = router;
