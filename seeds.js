const mongoose = require("mongoose");
const Restaurant = require("./models/restaurant");
const User = require("./models/user");

mongoose
  .connect("mongodb://localhost:27017/restaurantbackend", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Mongo Connection Open");
  })
  .catch(err => {
    console.log("Oh no mongo error!!!");
  });

const seedRestaurants = [
  {
    owner: "Mohit Kumar",
    name: "Haytt",
    city: "Dubai",
    diningPrice: 699,
    active: false,
  },
  {
    owner: "Digvijay",
    name: "The Taj",
    city: "San Francisco",
    diningPrice: 899,
    active: true,
  },
  {
    owner: "Adtiya",
    name: "The Miraj",
    city: "Egypt",
    diningPrice: 299,
    active: false,
  },
  {
    owner: "Ashish",
    name: "Chai & Sutta",
    city: "Delhi",
    diningPrice: 499,
    active: true,
  },
];

const seedUser = [
  {
    name: "Lovedeep",
    mail: "08love@gmail.com",
    password: "secure", // this is a seed db all other password will be hashed
    role: "admin",
  },
  {
    name: "Aditya",
    mail: "ad@gmail.com",
    password: "secure", // this is a seed db all other password will be hashed
    role: "owner",
  },
  {
    name: "Chirag",
    mail: "chirag@gmail.com",
    password: "secure", // this is a seed db all other password will be hashed
    role: "user",
  },
];
Restaurant.insertMany(seedRestaurants)
  .then(res => {
    console.log(console.log(res));
  })
  .catch(e => {
    console.log(e);
  });

User.insertMany(seedUser)
  .then(res => {
    console.log(console.log(res));
  })
  .catch(e => {
    console.log(e);
  });
