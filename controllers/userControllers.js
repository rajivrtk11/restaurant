const User = require("../models/user");
const Restaurant = require("../models/restaurant");

const Restaurant = require("../models/restaurant");
module.exports.updateUser = async (req, res) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(user);
  res.send("User Updated");
};
module.exports.filter = async (req, res) => {
  const { minPrice, maxPrice, city, name } = req.query;
  const restaurants = await Restaurant.find({
    diningPrice: { $gte: minPrice, $lte: maxPrice },
    city: city,
    name: name,
  });
  console.log(restaurants);
  res.send("Filtered Restaurants");
};
module.exports.getRestaurants = async (req, res) => {
  const { id } = req.params;
  const user = await User.findById(id);
  const restaurants = await Restaurant.find({});
  const activeRes = restaurants.filter(res => res.active === true);
  if (user.role === "owner") {
    console.log(user.restaurants);
    res.send.json({ restaurants: user.restaurants });
  } else if (user.role === "user") {
    console.log(activeRes);
    res.send.json({ restaurants: activeRes });
  } else if (user.role === "admin") {
    console.log(restaurants);
    res.send.json({ restaurants: restaurants });
  }
};
