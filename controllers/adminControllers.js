const Restaurant = require("../models/restaurant");
const User = require("../models/user");

module.exports.updateAdmin = async (req, res) => {
  const { id } = req.params;
  const user = await User.findByIdAndUpdate(id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(user);
  res.send("Admin Updated");
};
module.exports.updateUser = async (req, res) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(user);
  res.send("User Updated");
};

module.exports.deleteUser = async (req, res) => {
  const { adminId, id } = req.params;
  const user = await User.findById(id);
  const admin = await User.findById(adminId);
  if (user.role === "owner" && user.restaurants) {
    user.restaurants.map(res => {
      admin.restaurants.push(res);
      res.owner = admin;
    });
    await User.findByIdAndDelete(req.params.id);
  }
  await User.findByIdAndDelete(req.params.id);
  res.send("User Deleted");
};

module.exports.newUser = async (req, res) => {
  const { name, email, password, role } = req.body;
  const user = await User.create({ name, email, password, role });
  res.status(201).json({ user: user._id });
};
module.exports.updateRestaurant = async (req, res) => {
  const restra = await Restaurant.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(restra);
  res.send("User Updated");
};
module.exports.deleteRestaurant = async (req, res) => {
  await Restaurant.findByIdAndDelete(req.params.id);
  res.send("Restaurant Deleted");
};
module.exports.newRestaurant = async (req, res) => {
  const { id } = req.params;
  const { name, diningPrice, owner, city, active } = req.body;
  const user = await User.findById(id);
  const newRestra = new Restaurant(req.body);
  user.restaurants.push(newRestra);
  await user.save();
  await newRestra.save();
  res.send("Building your Restaurant");
};
