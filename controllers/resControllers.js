const Restaurant = require("../models/restaurant");
const User = require("../models/user");

module.exports.newRestaurants = async (req, res) => {
  const { id } = req.params;
  const { name, diningPrice, owner, city, active } = req.body;
  const user = await User.findById(id);
  const newRestra = new Restaurant(req.body);
  user.restaurants.push(newRestra);
  await user.save();
  await newRestra.save();
  res.send("Building your Restaurant");
};

module.exports.editRestaurants = async (req, res) => {
  const { id } = req.params;
  const restaurant = await Restaurant.findByIdAndUpdate(id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(restaurant);
  res.send("Restaurant Updated");
};

module.exports.deleteRestaurants = async (req, res) => {
  await Restaurant.findByIdAndDelete(req.params.id);
  console.log("Restaurant deleted");
  res.send("Restaurant Deleted");
};

module.exports.editOwner = async (req, res) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    runValidators: true,
    new: true,
  });
  console.log(user);
  res.send("Owner Updated");
};
